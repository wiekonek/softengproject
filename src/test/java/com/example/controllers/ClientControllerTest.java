package com.example.controllers;

import com.example.TestRoot;
import com.example.entities.Client;
import com.example.repositories.ClientRepository;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ClientControllerTest extends TestRoot {

    @Mock
    private ClientRepository clientRepositoryMock;

    @Autowired
    @InjectMocks
    private ClientController clientController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.reset(clientRepositoryMock);
        mockMvc = MockMvcBuilders.standaloneSetup(clientController).build();

        Client client = getNewTmpClient(1, "name", "lastname", "login");

        when(clientRepositoryMock.findOne(1)).thenReturn(client);

        when(clientRepositoryMock.exists(1)).thenReturn(true);
        when(clientRepositoryMock.exists(2)).thenReturn(false);
        when(clientRepositoryMock.findByLogin("login")).thenReturn(client);
    }

    @Test
    public void getClientBadRequest() throws Exception {
        mockMvc
                .perform(get("/api/clients/string"))
                .andExpect(status().isBadRequest());
        verifyZeroInteractions(clientRepositoryMock);
    }

    @Test(expected = NestedServletException.class)
    public void getClientByIdInvalid() throws Exception {
        mockMvc
                .perform(get("/api/clients/2"))
                .andExpect(status().isNotFound());
        verify(clientRepositoryMock, times(1)).exists(2);
        verifyNoMoreInteractions(clientRepositoryMock);
    }

    @Test
    public void getOneClientValid() throws Exception {
        mockMvc
                .perform(get("/api/clients/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.login", is("login")))
                .andExpect(jsonPath("$.firstname", is("name")))
                .andExpect(jsonPath("$.lastname", is("lastname")));
        verify(clientRepositoryMock, times(1)).exists(1);
        verify(clientRepositoryMock, times(1)).findOne(1);
        verifyNoMoreInteractions(clientRepositoryMock);
    }

    @Test
    public void addClientNotSupportedType() throws Exception {
        mockMvc
                .perform(post("/api/clients/")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .sessionAttr("Client", new Client()))
                .andExpect(status().isUnsupportedMediaType());
        verifyZeroInteractions(clientRepositoryMock);
    }

    @Test
    public void addClientValid() throws Exception {
        Gson gson = new Gson();
        Client newClient = getNewTmpClient(2, "name2", "lastname2", "login2");
        String json = gson.toJson(newClient);

        when(clientRepositoryMock.findByFirstnameAndLastname("name2", "lastname2")).thenReturn(null);
        when(clientRepositoryMock.findByLogin("login2")).thenReturn(null);

        mockMvc
                .perform(post("/api/clients/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
        verify(clientRepositoryMock, times(1)).save(any(Client.class));
    }

    @Test(expected = NestedServletException.class)
    public void addClientDuplicated() throws Exception {
        Gson gson = new Gson();
        Client newClient = getNewTmpClient(1, "name", "lastname", "login");
        String json = gson.toJson(newClient);

        when(clientRepositoryMock.findByFirstnameAndLastname("name", "lastname")).thenReturn(newClient);
        when(clientRepositoryMock.findByLogin("login")).thenReturn(newClient);

        mockMvc
                .perform(post("/api/clients/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk());
        verify(clientRepositoryMock, times(1)).save(any(Client.class));
    }

    @Test
    public void deleteClientInvalidRequest() throws Exception {
        mockMvc
                .perform(delete("/api/clients/qwe"))
                .andExpect(status().isBadRequest());
        verifyZeroInteractions(clientRepositoryMock);
    }

    @Test
    public void deleteClientByIdValid() throws Exception {
        mockMvc
                .perform(delete("/api/clients/1"))
                .andExpect(status().isOk());
        verify(clientRepositoryMock, times(1)).exists(1);
        verify(clientRepositoryMock, times(1)).delete(1);
        verifyNoMoreInteractions(clientRepositoryMock);
    }

    @Test( expected = NestedServletException.class)
    public void deleteClientByIdInvalid() throws Exception {
        mockMvc
                .perform(delete("/api/clients/2"))
                .andExpect(status().isNotFound());
        verifyZeroInteractions(clientRepositoryMock);
    }

    public static Client getNewTmpClient(int id, String name, String lastname, String login) {
        Client client = new Client();
        client.setId(id);
        client.setFirstname(name);
        client.setLastname(lastname);
        client.setLogin(login);
        client.setPassword("password");
        return client;
    }
}