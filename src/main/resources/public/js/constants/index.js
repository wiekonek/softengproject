export const MOVIES_URL = 'http://localhost:4000/api/movies';

export const OAUTH_TOKEN_URL = 'http://localhost:4000/oauth/token';
export const OAUTH_CLIENT_ID = 'SoftEngProjectClient';
export const OAUTH_CLIENT_SECRET = 'SoftEngProjectSecret';

export const START_SEARCHING = 'START_SEARCHING';
export const PROCESS_SEARCH_RESULTS = 'PROCESS_SEARCH_RESULTS';
export const SEARCH_RESULTS_READY = 'SEARCH_RESULTS_READY';
export const CANT_FETCH_RESULTS = 'CANT_FETCH_RESULTS';

export const FETCH_MOVIE_INFO = 'FETCH_MOVIE_INFO';
export const MOVIE_INFO_READY = 'MOVIE_INFO_READY';