import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';


export default class ResultItemComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { movieUri } = this.props;
        const { title, description, year, genres } = this.props.movieData;

        let annotation = year + " | " + genres;

        return (
          <li className="list-group-item result-item">
              <h4 className="list-group-item-heading">
                  <Link to={movieUri}>
                      <div className="result-item-title">{title}</div>
                  </Link>
                  <span className="result-item-annotation"><small>{annotation}</small></span>
              </h4>
              <p className="list-group-item-text">{description}</p>
          </li>
        );
    }
}

ResultItemComponent.propTypes = {
    movieData: PropTypes.object.isRequired
};