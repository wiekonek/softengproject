import React, { PropTypes, Component } from 'react';

export default class InputComponent extends Component {
    constructor(props) {
        super(props);
        this.searchTimeout = undefined;
        this.startSearching = this.startSearching.bind(this);
    }

    startSearching(text) {
        if (this.searchTimeout !== undefined)
            clearTimeout(this.searchTimeout);
        this.searchTimeout = setTimeout(() => this.props.onChange(text), 500);
    }

    render() {
        const { status, placeholder } = this.props;

        return (
            <div className="input-group input-group-lg input-component">
                <input className="form-control" type="text"
                       ref="inputRef" placeholder={placeholder}
                       onChange={() => { this.startSearching(this.refs.inputRef.value); }} />
                <span className="input-group-addon">
                    <div className="input-component-spinner">
                        {status === "searching" && <i className="fa fa-spinner fa-spin"></i>}
                    </div>
                </span>
            </div>
        );
    }
}

InputComponent.propTypes = {
    placeholder: PropTypes.string,
    onChange: PropTypes.func.isRequired
};