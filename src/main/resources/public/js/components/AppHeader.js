import React, { PropTypes, Component } from 'react';
import { Link } from 'react-router';

export default class AppHeaderComponent extends Component {
    render() {

        const { isLoggedIn } = this.props;

        return (
            <nav className="navbar navbar-custom" role="navigation">
                <div className="container">
                    <div className="navbar-header">
                        <Link to="/">
                            <div className="navbar-brand">
                                <i className="fa fa-film fa-2x" aria-hidden="true"></i>
                                <span>VOD</span>
                            </div>
                        </Link>
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                            <i className="fa fa-bars"></i>
                        </button>
                    </div>
                    <div className="collapse navbar-collapse navbar-right navbar-main-collapse">
                        <ul className="nav navbar-nav">
                            <li id="navbar-item-1">
                                <Link to="/">Browse movies</Link>
                            </li>
                            {isLoggedIn &&
                                <li id="navbar-item-2" className="dropdown">
                                    <a className="dropdown-toggle" data-toggle="dropdown" href="#">My account <span className="caret"></span></a>
                                    <ul className="dropdown-menu">
                                        <li><Link to="/shopping-cart">Shopping cart</Link></li>
                                        <li className="divider"></li>
                                        <li><Link to="/logout">Log out</Link></li>
                                    </ul>
                                </li>
                            }
                            {!isLoggedIn &&
                            <li id="navbar-item-2">
                                <Link to="/login">Log in</Link>
                            </li>
                            }
                            <hr />
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}
