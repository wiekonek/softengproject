import React, { Component, PropTypes } from 'react';
import ResultItemComponent from './ResultItem';

export default class ResultListComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { status, results } = this.props;

        let visibleResults = null;
        if (results["content"] !== undefined) {
            visibleResults = results.content.map((result, index) =>
                <ResultItemComponent key={index} movieUri={"/movies/" + result.id} movieData={result}/>
            );
        }

        let output = null;
        if (status !== "initial") {
            switch (status) {
                case "unavailable":
                    output = <div className="result-list"><div className="result-list-error">Can't fetch results</div></div>;
                    break;
                case "ready":
                    output = <ul className="list-group result-list">{visibleResults}</ul>;
                    break;
            }
        }

        return output;
    }
}

ResultListComponent.propTypes = {
    status: PropTypes.string.isRequired,
    results: PropTypes.object.isRequired
};