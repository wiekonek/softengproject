import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';


export default class LoginComponent extends Component {
    constructor(props) {
        super(props);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onFormSubmit() {
        let username = this.refs.usernameRef.value;
        let password = this.refs.passwordRef.value;

        this.props.issueLoginRequest(username, password);
    }

    render() {
        const { loggingError } = this.props;
        
        return (
            <div className="row">
                <div className="col-md-4 col-md-offset-4 col-xs-8">
                    <div className="panel panel-default login">
                        <div className="panel-heading">
                            <h4>Login</h4>
                        </div>
                        <div className="panel-body">
                            <form onSubmit={(event) => { event.preventDefault(); this.onFormSubmit(); }} role="form">
                                <input ref="usernameRef" type="text"
                                       className={loggingError ? "form-control error-input" : "form-control"} placeholder="Username" />
                                <input ref="passwordRef" type="password"
                                       className={loggingError ? "form-control error-input" : "form-control"} placeholder="Password" />
                                {loggingError && <div className="error-message">Invalid credentials</div>}
                                <button type="submit" className="pull-right btn btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

LoginComponent.propTypes = {
    loggingError: PropTypes.bool.isRequired,
    issueLoginRequest: PropTypes.func.isRequired
};

export default LoginComponent;
