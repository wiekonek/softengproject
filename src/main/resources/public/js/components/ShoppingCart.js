import React, { Component } from 'react';

export default class ShoppingCartComponent extends Component {
    constructor(props) {
        super(props);
        this.removeMovie = this.removeMovie.bind(this);
    }

    // componentDidMount() {
    //     if (localStorage.shoppingCart) {
    //         let shoppingCart = JSON.parse(localStorage.getItem("shoppingCart"));
    //         this.setState(shoppingCart);
    //     }
    // }

    removeMovie(movieId) {
        let shoppingCart = JSON.parse(localStorage.getItem("shoppingCart"));
        delete shoppingCart[movieId];
        localStorage.setItem("shoppingCart", JSON.stringify(shoppingCart));
        this.forceUpdate();
    }

    render() {

        let shoppingCart = JSON.parse(localStorage.getItem("shoppingCart"));

        let total = 0;
        let movieEntries = []
        for (let id in shoppingCart) {
            let price = shoppingCart[id]["price"];
            total += parseFloat(price.split(' ')[0]);

            let entry =
                <tr>
                    <td className="col-xs-2">
                        <img className="img-responsive" src={shoppingCart[id].imgUri} />
                    </td>
                    <td className="col-xs-5 text-center">
                        <div className="item-info">{shoppingCart[id].title}</div>
                    </td>
                    <td className="col-xs-3 text-center">
                        <div className="item-info">{shoppingCart[id].price}</div>
                    </td>
                    <td className="col-xs-2">
                        <div className="item-info-icon">
                            <a href="#" onClick={(event) => { event.preventDefault(); this.removeMovie(id); }}>
                                <i className="fa fa-times fa-lg" aria-hidden="true"></i>
                            </a>
                        </div>
                    </td>
                </tr>

            movieEntries.push(entry);
        }

        return (
            <div className="row">
                <div className="col-md-8 col-md-offset-2 col-xs-12">
                    <div className="panel panel-default shopping-cart">
                        <div className="panel-heading">
                            <h4>Shopping cart</h4>
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="shopping-cart-table">
                                    <table className="table">
                                        <thead>
                                        <tr>
                                            <th className="col-xs-2"></th>
                                            <th className="col-xs-5 text-center">Title</th>
                                            <th className="col-xs-3 text-center">Price</th>
                                            <th className="col-xs-2"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {movieEntries}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="pull-right shopping-cart-summary">
                                    <span className="summary-key">Total</span>
                                    <span className="summary-value">{total}</span>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer">
                            <button className="btn btn-primary pull-right">Proceed to checkout</button>
                            <div className="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}