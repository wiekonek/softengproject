import React, { Component, PropTypes } from 'react';
import { checkIfLoggedIn } from '../authentication';
import { Link } from 'react-router';

export default class MovieInfoDetailsComponent extends Component {


    render() {
        const { movieInfo, handleShoppingCart, isAlreadyAdded } = this.props;
        let isLoggedIn = checkIfLoggedIn();

        return (
            <div className="row">
                <div className="col-md-8 col-md-offset-2 col-xs-12">
                    <div className="panel panel-default movie-info">
                        <div className="panel-heading">
                            <h4>Information details</h4>
                        </div>
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-xs-3">
                                    <img className="img-responsive movie-info-image" src={movieInfo.imgUri} />
                                </div>
                                <div className="col-xs-9 movie-info-data">
                                    <div className="movie-info-description">{movieInfo.description}</div>
                                    <div className="movie-info-details">
                                        <div className="movie-info-details-entry">
                                            <span>Director:</span>
                                            <span>{movieInfo.director}</span>
                                        </div>
                                        <div className="movie-info-details-entry">
                                            <span>Writers:</span>
                                            <span>{movieInfo.writers}</span>
                                        </div>
                                        <div className="movie-info-details-entry">
                                            <span>Actors:</span>
                                            <span>{movieInfo.actors}</span>
                                        </div>
                                    </div>
                                    <div className="movie-info-actions">
                                        <div className="movie-info-actions-entry">
                                            <i className="fa fa-video-camera fa-lg" aria-hidden="true"></i>
                                            Watch the trailer
                                        </div>
                                        {isLoggedIn &&
                                            <div className="movie-info-actions-entry">
                                                {!isAlreadyAdded &&
                                                    <a href="#"
                                                       onClick={(event) => { event.preventDefault(); handleShoppingCart(); }}>
                                                        <i className="fa fa-shopping-cart fa-lg" aria-hidden="true"></i>
                                                        Add to your shopping cart
                                                    </a>
                                                }
                                                {isAlreadyAdded &&
                                                    <span className="already-added">This movie has been added to your shopping cart</span>
                                                }
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

MovieInfoDetailsComponent.propTypes = {
    movieInfo: PropTypes.object.isRequired,
    handleShoppingCart: PropTypes.func.isRequired
};
