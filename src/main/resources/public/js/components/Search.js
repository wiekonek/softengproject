import React, { Component, PropTypes } from 'react';
import InputComponent from './Input';
import ResultListContainer from '../containers/ResultList';

export default class SearchComponent extends Component {
    render() {
        const { placeholder, onChange, status, results, onMouseOver } = this.props;

        return (
            <div className="row">
                <div className="col-xs-6 col-xs-offset-3">
                    <h2 className="text-center">Search movies</h2>
                    <InputComponent status={status} placeholder={placeholder} onChange={onChange} />
                    <ResultListContainer status={status} results={results} />
                </div>
            </div>
        );
    }
}