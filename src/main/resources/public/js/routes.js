import React from 'react';
import { Route, IndexRoute } from 'react-router';
import AppContainer from './containers/App';
import SearchContainer from './containers/Search';
import MovieInfoContainer from './containers/MovieInfo';
import LoginContainer from './containers/Login';
import LogoutContainer from './containers/Logout';
import ShoppingCartComponent from './components/ShoppingCart';

export default (
    <Route path="/" component={ AppContainer }>
        <IndexRoute component={ SearchContainer } />
        <Route path="/login" component={ LoginContainer } />
        <Route path="/logout" component= { LogoutContainer } />
        <Route path="/shopping-cart" component = { ShoppingCartComponent } />
        <Route path="/movies/:movieId" component={ MovieInfoContainer } />
        <Route path="*" component={ SearchContainer } />
    </Route>
);