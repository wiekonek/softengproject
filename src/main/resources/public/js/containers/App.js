import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import AppHeaderComponent from '../components/AppHeader';
import { checkIfLoggedIn } from '../authentication';

export default class AppContainer extends Component {
    render() {
        
        const isLoggedIn = checkIfLoggedIn();
        
        return (
            <div>
                <AppHeaderComponent isLoggedIn={isLoggedIn}/>
                <div className="container">
                    <ReactCSSTransitionGroup transitionName="main-transition"
                                             transitionEnterTimeout={150}
                                             transitionLeaveTimeout={150}>
                        {React.cloneElement(this.props.children, {
                            key: this.props.location.pathname
                        })}
                    </ReactCSSTransitionGroup>
                </div>
            </div>
        );
    }
}