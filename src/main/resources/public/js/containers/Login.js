import React, { Component, PropTypes } from 'react';
import LoginComponent from '../components/Login';
import { browserHistory } from 'react-router';
import { issueAuthenticationRequest } from '../authentication';

export default class LoginContainer extends Component {
    constructor(props) {
        super(props);
        this.state = { loggingError: false };
        this.issueLoginRequest = this.issueLoginRequest.bind(this);
    }

    issueLoginRequest(username, password) {

        if (username.length == 0 || password.length == 0) {
            this.setState({ loggingError: true });
        }
        else {
            this.setState({loggingError: true});
            let token = issueAuthenticationRequest(username, password);
            if (Object.keys(token).length !== 0) {
                this.setState({ loggingError: false });
                localStorage.setItem('token', token);
                browserHistory.push("/");
            }
            else {
                this.setState({ loggingError: true });
            }
        }
    }

    render() {

        return (
            <LoginComponent  loggingError={this.state.loggingError} issueLoginRequest={this.issueLoginRequest} />
        );
    }
}

export default LoginContainer;