import React, { Component } from 'react';
import { browserHistory } from 'react-router';

export default class LogoutContainer extends Component {
    constructor(props) {
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
        localStorage.removeItem('token');
        localStorage.removeItem('shoppingCart');
        browserHistory.push('/');
    }

    render() {

        return null;
    }
}

export default LogoutContainer;