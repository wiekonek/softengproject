import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import ResultListComponent from '../components/ResultList';

class ResultListContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { status, results } = this.props;

        return (
            <ResultListComponent status={status}
                                 results={results} />
        );
    }
}

ResultListContainer.propTypes = {
    status: PropTypes.string.isRequired,
    results: PropTypes.object.isRequired
};

ResultListContainer.defaultProps = {
    status: "initial",
    results: {}
};

export default ResultListContainer