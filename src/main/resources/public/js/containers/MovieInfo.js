import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import MovieInfoDetailsComponent from '../components/MovieInfoDetails';
import { getMovieInfo } from '../actions';

export default class MovieInfoContainer extends Component {
    constructor(props) {
        super(props);
        this.checkIfAlreadyAdded = this.checkIfAlreadyAdded.bind(this);
        this.handleShoppingCart = this.handleShoppingCart.bind(this);
    }

    checkIfAlreadyAdded() {
        const { movieInfo } = this.props.movieInfo;

        if (localStorage.shoppingCart) {
            let shoppingCart = JSON.parse(localStorage.getItem("shoppingCart"));
            for (let id in shoppingCart) {
                if (parseInt(id) === movieInfo.id) {
                    return true;
                }
            }
        }

        return false;
    }

    componentDidMount() {
        this.props.dispatch(getMovieInfo(this.props.params.movieId));
    }

    handleShoppingCart() {
        const {movieInfo} = this.props.movieInfo;

        if (!localStorage.shoppingCart) {
            let shoppingCart = {};
            localStorage.setItem("shoppingCart", JSON.stringify(shoppingCart));
        }

        let shoppingCart = JSON.parse(localStorage.getItem("shoppingCart"));
        let toAdd = {}
        toAdd[movieInfo.id] = {
            id: movieInfo.id,
            imgUri: movieInfo.imgUri,
            title: movieInfo.title,
            price: "10 PLN"
        };
        let updatedShoppingCart = Object.assign({}, shoppingCart, toAdd);
        
        localStorage.setItem("shoppingCart", JSON.stringify(updatedShoppingCart));
        this.forceUpdate();
    }

    render() {
        const {movieInfo} = this.props.movieInfo;
        let isAlreadyAdded = this.checkIfAlreadyAdded();

        return (
            <MovieInfoDetailsComponent movieInfo={movieInfo}
                                       handleShoppingCart={this.handleShoppingCart}
                                       isAlreadyAdded={isAlreadyAdded} />
        );
    }
}

MovieInfoContainer.propTypes = {
    movieInfo: PropTypes.object.isRequired
};

MovieInfoContainer.defaultProps = {
    movieInfo: {}
};

const mapStateToProps = (state) => {
    return {
        movieInfo: state.movieInfo
    }
};

MovieInfoContainer = connect(mapStateToProps, null)(MovieInfoContainer)
export default MovieInfoContainer