import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import SearchComponent from '../components/Search';
import { getSearchResults } from '../actions';


class SearchContainer extends Component {
    constructor(props) {
        super(props);
        this.startSearching = this.startSearching.bind(this);
    }

    startSearching(text) {
        this.props.dispatch(getSearchResults(text));
    }

    render() {
        const { status, results } = this.props;

        return (
            <SearchComponent placeholder={"type the query"}
                             onChange={this.startSearching}
                             status={status}
                             results={results} />
        );
    }
}

SearchContainer.propTypes = {
    status: PropTypes.string.isRequired,
    results: PropTypes.object.isRequired
};

SearchContainer.defaultProps = {
    status: "initial",
    results: {}
};

const mapStateToProps = (state) => {
    return {
        status: state.searchResult.status,
        results: state.searchResult.results
    }
};

SearchContainer = connect(mapStateToProps, null)(SearchContainer)
export default SearchContainer