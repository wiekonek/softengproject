import * as types from '../constants';
import fetch from 'isomorphic-fetch';

export const getSearchResults = (text) => {
    return dispatch => {
        dispatch({ type: types.START_SEARCHING });
        return  fetch(types.MOVIES_URL + '?search=' + text + '&size=5')
                .then(response => response.json())
                .then(json => dispatch(searchResultsReady(json)))
                .catch(error => dispatch({ type: types.CANT_FETCH_RESULTS }));
    }
};

const searchResultsReady = (results) => {
    return {
        type: types.SEARCH_RESULTS_READY,
        results
    };
};

export const getMovieInfo = (movieId) => {
    return dispatch => {
        dispatch({ type: types.FETCH_MOVIE_INFO });
        return  fetch(types.MOVIES_URL + '/' + movieId)
                .then(response => response.json())
                .then(json => dispatch(movieInfoReady(json)));
    };
};

const movieInfoReady = (movieInfo) => {
    return {
            type: types.MOVIE_INFO_READY,
            movieInfo
        };
};