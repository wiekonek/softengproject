import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import searchResult from './searchResult';
import movieInfo from './movieInfo';

const rootReducer = combineReducers({
    searchResult,
    movieInfo,
    routing: routerReducer
});

export default rootReducer;