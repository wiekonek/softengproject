import * as types from '../constants';

const searchResult = (state = { searchResult: { status: "initial" } }, action) => {
    switch (action.type) {
        case types.START_SEARCHING:
            return Object.assign({}, state, {
                status: "searching"
            });
        case types.CANT_FETCH_RESULTS:
            return Object.assign({}, state, {
                status: "unavailable"
            });
        case types.SEARCH_RESULTS_READY:
            return Object.assign({}, state, {
                status: "ready",
                results: action.results
            });
        default:
            return state;
    }
}

export default searchResult;