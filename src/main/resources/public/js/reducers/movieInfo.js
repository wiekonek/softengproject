import * as types from '../constants';

const moviesInfo = (state = { movieInfo: {} }, action) => {
    switch (action.type) {
        case types.FETCH_MOVIE_INFO:
            return Object.assign({}, state, {
                status: "pending"
            });
        case types.MOVIE_INFO_READY:
            console.log("killa", action);
            return Object.assign({}, state, {
                movieInfo: action.movieInfo
            });
        default:
            return state;
    }
}

export default moviesInfo;