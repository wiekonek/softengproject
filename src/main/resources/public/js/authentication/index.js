import * as types from '../constants';
import jQuery from 'jquery';

export const checkIfLoggedIn = () => {
    if (!localStorage.getItem('token')) {
        return false;
    }
    return true;
}

export const issueAuthenticationRequest = (username, password) => {
    var authorizationBasic = window.btoa(types.OAUTH_CLIENT_ID + ':' + types.OAUTH_CLIENT_SECRET);
    var token = {}
    jQuery.ajax({
        type: 'POST',
        // url: types.OAUTH_TOKEN_URL,
        url: 'http://SoftEngProjectClient:SoftEngProjectSecret@localhost:4000/oauth/token',
        data: { username, password, grant_type: 'password', scope: 'read write' },
        // dataType: "json",
        contentType: 'application/x-www-form-urlencoded',
        async: false,
        xhrFields: {
            withCredentials: true
        },
        // crossDomain: true,
        headers: {
            'Authorization': 'Basic ' + authorizationBasic
        },
        //beforeSend: function (xhr) {
        //},
        success: function (result) {
            token = result.access_token;
        },
        //complete: function (jqXHR, textStatus) {
        //},
        error: function (req, status, error) {
            console.log("Error while logging in ", error);
        }
    });
    return token;
}