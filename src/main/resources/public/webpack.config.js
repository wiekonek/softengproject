var path    = require('path'),
	webpack = require('webpack'),
	WatchIgnorePlugin = require('watch-ignore-webpack-plugin');

module.exports = {
	entry: [
		'webpack-dev-server/client?http://localhost:8080', // WebpackDevServer host and port
		'webpack/hot/only-dev-server',
		'babel-polyfill',
		'bootstrap-loader',
		'./js/app'
		// 'bootstrap-webpack!./bootstrap.config.js',
		// 'font-awesome-webpack!./font-awesome.config.js'
    ],
	output: {
		filename: "build/bundle.js"
	},
	devtool: 'source-map',
	plugins: [
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		}),
		new WatchIgnorePlugin([
			path.resolve(__dirname, './node_modules/'),
		]),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),

		// example of polyfilling with webpack 
		// OR just include the babel runtime option below and get Promises, Generators, Map, and much more!
		// You can even get forward looking proposed features for ES7 and beyond with the 
		// stage query parameter below https://babeljs.io/docs/usage/experimental/ welcome
		// to the future of JavaScript! :)
		//
		//new webpack.ProvidePlugin({
		//	'arrayutils': 'imports?this=>global!exports?global.arrayutils!arrayutils'
		//})
	],
	resolve: {
		// require files in app without specifying extensions
		extensions: ['', '.js', '.json', '.jsx', '.less'],
		alias: {
			// pretty useful to have a starting point in nested modules
			'appRoot': path.join(__dirname, 'js'),
			'vendor': 'appRoot/vendor'
		}
	},
    module: {
        loaders: [
            // {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff"},
            // {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,  loader: "url?limit=10000&mimetype=application/octet-stream"},
            // {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,  loader: "file"},
			// { test: /\.less$/,      loader: 'style-loader!css-loader!autoprefixer?browsers=last 2 version!less-loader' },
            // {
             //    test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
             //    loader: "url-loader?limit=10000&mimetype=application/font-woff"
            // },{
             //    test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
             //    loader: "file-loader"
            // },
			// {
			// 	test: /\.scss$/,
			// 	exclude: /node_modules/,
			// 	loader: "style-loader!css-loader!sass-loader"
			// },
			// { test: /\.css$/,       loader: 'style-loader!css-loader' },
			// { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192'}, // inline base64 URLs for <=8k images, direct URLs for the rest

			// { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' },
			// { test: /\.jsx?$/, exclude: /(node_modules|bower_components)/, loader: 'babel' },
			// { test: /\.css$/, loader: 'style-loader!css-loader' }, //, include: path.join(__dirname, 'src/css')},
			// { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
			// { test: /\.(woff|woff2)$/, loader:"url?prefix=font/&limit=5000" },
			// { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
			// { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" },
            //
			// { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&minetype=application/font-woff" },
			// { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },

			// { test: /\.less$/, loader: 'style-loader!css-loader!less-loader'},
			{
				test: /\.scss$/,
				exclude: /node_modules/,
				loader: "style-loader!css-loader!sass-loader"
			},
			{ test: /\.css$/, loader: 'style-loader!css-loader' },
			{ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
			{ test: /\.(woff|woff2)$/, loader:"url?prefix=font/&limit=5000" },
			{ test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
			{ test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" },
			{
				test: /\.jsx?$/, 
				include: [
					path.join(__dirname, 'js'), // files to apply this loader to
					path.join(__dirname, 'node_modules/reflux-core')
				], 
				// http://jamesknelson.com/using-es6-in-the-browser-with-babel-6-and-webpack/
				loaders: ['react-hot', 'babel?presets[]=react,presets[]=es2015', 'reflux-wrap-loader'] // loaders process from right to left
			},
        ]
    }
};
