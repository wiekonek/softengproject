create table MoviePerson (
	movieId int not null,
    personId int not null,
    constraint fkMoviePersonMovieId foreign key(movieId) references Movie(id),
    constraint fkMoviePersonPersonId foreign key(personId) references Person(id),
    primary key(movieId, personId)
);