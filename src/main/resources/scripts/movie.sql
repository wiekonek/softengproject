create table Movie (
	id int auto_increment,
    title varchar(100),
    year int,
    trailerUri varchar(255),
    fullVersionUri varchar(255),
    primary key(id)
);