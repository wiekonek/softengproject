create table Person (
	id int auto_increment,
    firstname varchar(100),
    lastname varchar(100),
    type varchar(30),
    primary key(id)
);