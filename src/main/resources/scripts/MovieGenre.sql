create table MovieGenre (
	movieId int not null,
    genreId int not null,
    constraint fkMovieGenreMovieId foreign key(movieId) references Movie(id),
    constraint fkMovieGenreGenreId foreign key(genreId) references Genre(id),
    primary key(movieId, genreId)
);