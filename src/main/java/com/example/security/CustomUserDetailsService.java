package com.example.security;

import com.example.entities.Client;
import com.example.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom class responsible for loading
 * user-specific data used in the authentication process
 * @see UserDetailsService
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private ClientRepository clientRepository;

    /**
     * Fetches user, security-related information
     * @param username Username
     * @return An object holding crucial security-related information about the user
     * @throws UsernameNotFoundException in case of fetching a non-existent user
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Client client = clientRepository.findByLogin(username);
        if (client == null) {
            throw new UsernameNotFoundException("Such a Client (login = " + username + ") doesn't exist");
        }

        List<GrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(new SimpleGrantedAuthority("ROLE_CLIENT"));

        UserDetails userDetails = new User(client.getLogin(), client.getPassword(), authorityList);
        return userDetails;
    }
}
