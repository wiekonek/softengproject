package com.example.controllers;

import com.example.validation.ResourceNotFoundException;
import org.apache.http.HttpResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * Controller responsible for handling general requests i.e. not
 * specific to the domain model
 */
@RestController
@RequestMapping("/")
public class MainController {

    /**
     * Global handler for any requests that result in 404 errors
     */
    @RequestMapping(value = "/notFound")
    public void notFoundHandler() throws ResourceNotFoundException {
        throw new ResourceNotFoundException("The requested resource could not be found");
    }
}
