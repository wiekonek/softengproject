package com.example.exceptions;

import com.example.validation.ErrorResponse;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

/**
 * Exception handler responsible for handling
 * exceptions which occur at the persistence layer
 * @see NoSuchEntityException
 * @see DataIntegrityViolationException
 */
@ControllerAdvice
public class DataAccessExceptionHandler {

    /**
     * Handles {@link DataAccessException} exceptions
     * and returns a response containing the relevant
     * status code in the form of {@link ErrorResponse} object
     * @param ex The exception object
     * @return A response entity which encapsulates {@link ErrorResponse}
     * @see ErrorResponse
     */
    @ExceptionHandler({
            NoSuchEntityException.class,
            DataIntegrityViolationException.class
    })
    public ResponseEntity<ErrorResponse> handleDataAccessException(DataAccessException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setMessage(ex.getMessage());
        errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        errorResponse.setTimestamp(new Date());

        return new ResponseEntity<ErrorResponse>(errorResponse, null, HttpStatus.BAD_REQUEST);
    }
}
