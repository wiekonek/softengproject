package com.example.exceptions;

import org.springframework.dao.DataAccessException;

/**
 * Thrown as a result of attempting to fetch a non-existent entity
 * Usually results in a 404 error status code
 */
public class NoSuchEntityException extends DataAccessException {

    public NoSuchEntityException(String message) {
        super(message);
    }
}
