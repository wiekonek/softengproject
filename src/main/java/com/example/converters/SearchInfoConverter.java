package com.example.converters;

import org.springframework.core.convert.converter.Converter;

/**
 * Class responsible for converting a request parameter
 * to a SearchInfo object
 */
public class SearchInfoConverter implements Converter<String, SearchInfo> {

    @Override
    public SearchInfo convert(String query) {
        return new SearchInfo(query);
    }
}
