package com.example.repositories;

import com.example.entities.Movie;
import com.example.entities.MovieOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MovieOrderRepository extends PagingAndSortingRepository<MovieOrder, Integer> {
}
