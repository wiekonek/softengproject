package com.example.repositories;

import com.example.entities.Client;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ClientRepository extends PagingAndSortingRepository<Client, Integer> {

    Client findByLogin(String login);
    Client findByFirstnameAndLastname(String firstname, String lastname);
}
