package com.example.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Repositories extending this interface provide
 * the means of full-text searching their entities
 */
public interface FullTextSearchableRepository<T> {

    /**
     * Searchers (full-text search) for the query text among its entities
     * @param query The text to be searched for
     * @param pageable An interface which encapsulates pagination information
     * @return A {@link Page} object containing entities which, in turn, contain the query text
     */
    Page<T> search(String query, Pageable pageable);
}
