package com.example.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
// it was supposed to be called just 'Order' but
// that's a keyword in some databases
@Table(name = "movieorder")
public class MovieOrder {

    public enum OrderStatus { UNFULFILLED, FULFILLED }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "clientId")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "movieId")
    private Movie movie;

    private Date date;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }
}
